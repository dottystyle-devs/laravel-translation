<?php

namespace Dottystyle\LaravelTranslation;

use Illuminate\Translation\Translator as BaseTranslator;

class Translator extends BaseTranslator
{
    /**
     * @var array 
     */
    protected $defaultReplacements = [];

    /**
     * Make the place-holder replacements on a line.
     *
     * @param  string  $line
     * @param  array   $replace
     * @return string
     */
    protected function makeReplacements($line, array $replace)
    {
        return parent::makeReplacements($line, $replace + $this->defaultReplacements);
    }
    
    /**
     * Add default placeholder replacement values.
     * 
     * @param string|array $key
     * @param mixed $value (optional)
     * @return $this
     */
    public function addDefaultReplacement($key, $value = null)
    {
        if (is_array($key)) {
            $this->defaultReplacements = array_merge($this->defaultReplacements, $key);
        } else {
            $this->defaultReplacements[$key] = $value;
        }
        
        return $this;
    }

    /**
     * Get the default placeholder value for the given key.
     * 
     * @param string $key
     * @return mixed
     */
    public function getDefaultReplacement($key)
    {
        return $this->defaultReplacements[$key] ?? null;
    }
}
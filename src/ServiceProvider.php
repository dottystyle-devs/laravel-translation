<?php

namespace Dottystyle\LaravelTranslation;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Boot the application services.
     * 
     * @return void
     */
    public function boot()
    {
        $this->app->extend('translator', function ($translator) {
            $newTranslator = new Translator($translator->getLoader(), $translator->getLocale());
            $newTranslator->setFallback($translator->getFallback());

            return $newTranslator;
        });
    }

    /**
     * Register services to the container.
     * 
     * @return void
     */
    public function register()
    {
    }
}